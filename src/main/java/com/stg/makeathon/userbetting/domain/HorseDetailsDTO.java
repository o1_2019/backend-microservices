package com.stg.makeathon.userbetting.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class HorseDetailsDTO {
    @JsonProperty("horse_id")
    private int horseId;

    @JsonProperty("horse_name")
    private String horseName;

    @JsonProperty("last_5_races")
    private List<Double> last5races;

    @JsonProperty("win_multiplier")
    private double winMultiplier;

    @JsonProperty("lose_multiplier")
    private double loseMultiplier;
}
