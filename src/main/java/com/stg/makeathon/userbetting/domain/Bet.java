package com.stg.makeathon.userbetting.domain;

public class Bet {
    String userId;
    double placed;
    double win_multiplier;
    double lose_multiplier ;
    int horse_id;
    int place_predicted;
    int race_id;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public double getPlaced() {
        return placed;
    }

    public void setPlaced(double placed) {
        this.placed = placed;
    }

    public double getWin_multiplier() {
        return win_multiplier;
    }

    public void setWin_multiplier(double win_multiplier) {
        this.win_multiplier = win_multiplier;
    }

    public double getLose_multiplier() {
        return lose_multiplier;
    }

    public void setLose_multiplier(double lose_multiplier) {
        this.lose_multiplier = lose_multiplier;
    }

    public int getHorse_id() {
        return horse_id;
    }

    public void setHorse_id(int horse_id) {
        this.horse_id = horse_id;
    }

    public int getPlace_predicted() {
        return place_predicted;
    }

    public void setPlace_predicted(int place_predicted) {
        this.place_predicted = place_predicted;
    }

    public int getRace_id() {
        return race_id;
    }

    public void setRace_id(int race_id) {
        this.race_id = race_id;
    }
}
