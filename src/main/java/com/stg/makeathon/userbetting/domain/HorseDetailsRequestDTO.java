package com.stg.makeathon.userbetting.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class HorseDetailsRequestDTO {
    @JsonProperty("horses")
    private List<String> horses;

    public List<Integer> getHorses() {
        return horses.stream().map(Integer::parseInt).collect(Collectors.toList());
    }

    public void setHorses(List<String> horses) {
        this.horses = horses;
    }

    @Override
    public String toString() {
        return "ClassPojo [horses = " + horses + "]";
    }
}
