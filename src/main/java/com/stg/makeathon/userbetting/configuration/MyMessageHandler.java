package com.stg.makeathon.userbetting.configuration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.stg.makeathon.userbetting.service.RaceSearchService;

@Component
public class MyMessageHandler extends TextWebSocketHandler {
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        // The WebSocket has been closed
    }
    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        // The WebSocket has been opened
        // I might save this session object so that I can send messages to it outside of this method
        // Let's send the first message
//        session.sendMessage(new TextMessage("You are now connected to the server. This is the first message."));
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage textMessage) throws Exception {
        // A message has been received
    	//session.sendMessage(new TextMessage("Sent"));
    	RaceSearchService raceSearchService = new RaceSearchService();
    	Map<String,Object> response = new HashMap<String,Object>();
		JSONObject responseJson = new JSONObject();

		try{
		 List<Map<String,Object>> raceDetails = raceSearchService.getAllRaces();
		 responseJson.put("response", JSONObject.valueToString(raceDetails));
		 response.put("response", raceDetails);
		 session.sendMessage(new TextMessage(responseJson.toString()));
		}
		catch(Exception e){
			System.out.println("exception encountered");
		}
        System.out.println("Message received: " + textMessage.getPayload());
    }
}
