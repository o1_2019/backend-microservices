package com.stg.makeathon.userbetting.configuration;

import java.util.logging.SocketHandler;

import com.stg.makeathon.userbetting.socket.OddsSocketHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;


@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {
    @Bean
    public WebSocketHandler myMessageHandler() {
        return new MyMessageHandler();
    }

    @Bean
    public OddsSocketHandler oddsSocketHandler() {
        return new OddsSocketHandler();
    }

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(myMessageHandler(), "/details/race/all/socket").setAllowedOrigins("*");
        registry.addHandler(oddsSocketHandler(), "/socket/details/horse").setAllowedOrigins("*");
    }
}
