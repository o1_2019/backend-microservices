package com.stg.makeathon.userbetting.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.stg.makeathon.userbetting.service.PaypalClient;

@RestController
@RequestMapping(value = "/api/paypal")
@CrossOrigin()
public class PaypalController {

    private final PaypalClient payPalClient;
    @Autowired
    PaypalController(PaypalClient payPalClient){
        this.payPalClient = payPalClient;
    }

    @PostMapping(value = "/make/payment")
    public Map<String, Object> makePayment(@RequestParam("sum") String sum){
        return payPalClient.createPayment(sum);
    }
    
    @PostMapping(value = "/complete/payment")
    public Map<String, Object> completePayment(HttpServletRequest request){
        return payPalClient.completePayment(request);
    }
}
