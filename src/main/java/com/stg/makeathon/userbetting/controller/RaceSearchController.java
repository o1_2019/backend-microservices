package com.stg.makeathon.userbetting.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.stg.makeathon.userbetting.service.RaceSearchService;

@RestController
@CrossOrigin()
@RequestMapping(value = "/api")
public class RaceSearchController {

	@Autowired
	RaceSearchService raceSearchService;


	@GetMapping(value = "/details/race/all")
	public ResponseEntity<Map<String,Object>> getAllRaces(){

		Map<String,Object> response = new HashMap<String,Object>();
		try{
		 List<Map<String,Object>> raceDetails = raceSearchService.getAllRaces();
		 response.put("response", raceDetails);
		}catch(Exception e){
			   response.put("error", "Internal Server Error");
			   return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}


	@GetMapping(value = "/details/race")
	public ResponseEntity<Map<String,Object>> getRaceDetails(@RequestParam("raceId") String raceId){

		Map<String,Object> response = new HashMap<String,Object>();

		if(raceId==null || raceId.isEmpty()){
			raceId = "1";
		}
		Integer race_id;
		try{
		 race_id = Integer.parseInt(raceId);
		}catch(Exception e){
		   response.put("error", "Invalid race id passed");
		   return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		try{
		 response = raceSearchService.getRaceDetails(race_id);
		}catch(Exception e){
			   response.put("error", "Internal Server Error");
			   return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping(value = "/details/horse")
	public ResponseEntity<Map<String,Object>> getHorseDetails(@RequestBody Map<String,Object> horses){

		Map<String,Object> response = new HashMap<String,Object>();
		List<Integer> horse_ids;
		try{
		 horse_ids = (List<Integer>)horses.get("horse_ids");
		}catch(Exception e){
			 response.put("error", "Invalid request");
			  return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}

		try{
		 List<Map<String,Object>> horseDetails = raceSearchService.getHorseDetails(horse_ids);
		 response.put("horse_details", horseDetails);
		}catch(Exception e){
			   e.printStackTrace();
			   response.put("error", "Bad Request");
			   return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@GetMapping(value = "/details/user")
	public ResponseEntity<Map<String,Object>> getUserDetails(@RequestParam("userId") String userId){

		Map<String,Object> response = new HashMap<>();
		try{
//		 user_id = Integer.parseInt(userId);
		}catch(Exception e){
			 response.put("error", "Invalid request");
			  return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}

		try{
		Map<String,Object> userDetails = raceSearchService.getUserDetails(userId);
		 response.put("userDetails", userDetails);
		}catch(Exception e){
			   response.put("error", "Bad Request");
			   return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping(value = "/purchase/user")
	public ResponseEntity<Map<String,Object>> userPurchaseDetails(@RequestBody Map<String,Object> betDetails){

		Map<String,Object> response = new HashMap<String,Object>();
		List<Map<String, Object>> orders = (List<Map<String, Object>>) betDetails.get("betsPlaced");

		System.out.println(betDetails.toString());
		try{
			for (int i=0; i<orders.size(); i++) {
				Map<String, Object> order = orders.get(i);
				System.out.println(order.get("value").toString());
				raceSearchService.userPurchase((Map<String, Object>) order.get("value"), betDetails.get("id").toString());
			}
			response.put("transaction", "success");
			response.put("error", null);
		}catch(Exception e){
			   e.printStackTrace();
			   response.put("error", "Bad Request");
			   return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}


}
