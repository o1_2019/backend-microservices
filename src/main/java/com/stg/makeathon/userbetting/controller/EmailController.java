package com.stg.makeathon.userbetting.controller;


import com.stg.makeathon.userbetting.service.MailClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/email")
@CrossOrigin()
public class EmailController {

    @Autowired
    public MailClient mailClient;

    @GetMapping(value = "/send")
    public ResponseEntity<Map<String, Object>> sendEmail(@RequestParam("userId") Integer userId, @RequestParam("betId") String betId) {

        Map<String, Object> response = new HashMap<>();

//        mailClient.prepareAndSend("madhu.aithal23@gmail.com", "sample message");

        response.put("status", "Success");
        response.put("error", false);

        return new ResponseEntity<>(response, HttpStatus.OK);

    }
}
