package com.stg.makeathon.userbetting.service;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.stg.makeathon.userbetting.domain.Bet;
import com.stg.makeathon.userbetting.utills.KafkaProducer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;


@Service
public class RaceSearchService {

    @Autowired
    KafkaProducer producer;

    @Autowired
    public MailClient mailClient;

    public List<Map<String, Object>> getAllRaces() throws UnknownHostException {
        MongoClient mongo = new MongoClient("localhost", 27017);
        DB db = mongo.getDB("makeathon-db");
        DBCollection table = db.getCollection("race_details");

        BasicDBObject searchQuery = new BasicDBObject();


        //Object obj = new Object();
//		Map<String,Object> startTimeMap = new HashMap<>();
//		Map<String,Object> endTimeMap = new HashMap<>();
//		
//		startTimeMap.put("$lte", new Date());
//		endTimeMap.put("gte", new Date());
//		
        Date timeNow = new Date();

        searchQuery.put("startTime", BasicDBObjectBuilder.start("$gte", timeNow).get());
        searchQuery.put("endTime", BasicDBObjectBuilder.start("$gte", timeNow).get());

        DBCursor cursor = table.find(searchQuery);
        List<Map<String, Object>> raceDetails = new ArrayList<Map<String, Object>>();

        while (cursor.hasNext()) {
            DBObject record = cursor.next();
            raceDetails.add(record.toMap());
        }

        return raceDetails;
    }

    public Map<String, Object> getRaceDetails(Integer race_id) throws UnknownHostException {
        MongoClient mongo = new MongoClient("localhost", 27017);
        DB db = mongo.getDB("makeathon-db");
        DBCollection table = db.getCollection("race_details");

        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("raceId", race_id);

        DBObject cursor = table.findOne(searchQuery);

        Map<String, Object> raceDetails = cursor.toMap();

        return raceDetails;
    }

    public List<Map<String, Object>> getHorseDetails(List<Integer> horseIds) throws UnknownHostException {
        MongoClient mongo = new MongoClient("localhost", 27017);
        DB db = mongo.getDB("makeathon-db");
        DBCollection table = db.getCollection("horse_details");
        List<Map<String, Object>> horses = new ArrayList<>();

        for (Integer i : horseIds) {

            BasicDBObject searchQuery = new BasicDBObject();
            searchQuery.put("horse_id", i);

            DBObject cursor = table.findOne(searchQuery);

            Map<String, Object> horseDetails = cursor.toMap();

            List<Double> last_5_races = (List<Double>) horseDetails.get("last_5_races");

            Double last_race = last_5_races.get(4);

            Double win_multiplier = last_race * 1;
            Double lose_multiplier = (1 / last_race);

            horseDetails.put("win_multiplier", win_multiplier);
            horseDetails.put("lose_multiplier", lose_multiplier);

            horses.add(horseDetails);

        }
        return horses;
    }

    public Map<String, Object> getUserDetails(String userId) throws UnknownHostException {
        MongoClient mongo = new MongoClient("localhost", 27017);
        DB db = mongo.getDB("makeathon-db");
        DBCollection table = db.getCollection("user_details");
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("id", userId);
        DBObject cursor = table.findOne(searchQuery);
        Map<String, Object> userDetails = cursor.toMap();
        List<Map<String, Object>> betsProcessedList = (ArrayList<Map<String, Object>>) userDetails.get("bets_processed");
        betsProcessedList = getHorseName(betsProcessedList, db);
        userDetails.put("bets_processed", betsProcessedList);

        List<Map<String, Object>> betsPlacedList = (ArrayList<Map<String, Object>>) userDetails.get("bets_placed");
        getHorseName(betsPlacedList, db);
        userDetails.put("bets_placed", betsPlacedList);
        mongo.close();
        return userDetails;
    }

    private List<Map<String, Object>> getHorseName(List<Map<String, Object>> mapList, DB db) {
        DBCollection table = db.getCollection("horse_details");
        for (int i = 0; i < mapList.size(); i++) {
            BasicDBObject horseQuery = new BasicDBObject();
            horseQuery.put("horse_id", mapList.get(i).get("horse_id"));
            DBObject horseCursor = table.findOne(horseQuery);
            Map<String, Object> horseDetails = horseCursor.toMap();
            mapList.get(i).put("horse_name", horseDetails.get("horse_name"));
        }
        return mapList;
    }

    public boolean sendEmail(Map<String, Object> details) {
        System.out.println(details.toString());
//        mailClient.prepareAndSend(details.get("id").toString(), details);
        mailClient.prepareAndSend("madhu.aithal23@gmail.com", details);
        return true;
    }

    public boolean userPurchase(Map<String, Object> user_request, String userId) throws UnknownHostException, Exception {
        MongoClient mongo = new MongoClient("localhost", 27017);
        DB db = mongo.getDB("makeathon-db");
        DBCollection table = db.getCollection("user_details");

//        String userId = user_request.get("id").toString();

        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("id", userId);

        DBObject cursor = table.findOne(searchQuery);

        Map<String, Object> userDetails = cursor.toMap();

        List<Map<String, Object>> bets_placed = (List<Map<String, Object>>) userDetails.get("bets_placed");
        Map<String, Object> bets = new HashMap<String, Object>();

        Double placed = Double.valueOf(user_request.get("betAmount").toString());
        Double win_multiplier = Double.valueOf(user_request.get("win_multiplier").toString());
        Double lose_multiplier = Double.valueOf(user_request.get("lose_multiplier").toString());
        Integer horse_id = Integer.valueOf(user_request.get("horse_id").toString());
//        Integer place_predicted = Integer.valueOf(user_request.get("place_predicted").toString());
        Integer race_id = Integer.valueOf(user_request.get("race_id").toString());
        //Kafka Writing
        Bet bet = new Bet();
        bet.setHorse_id(horse_id);
        bet.setLose_multiplier(lose_multiplier);
        bet.setHorse_id(horse_id);
//        bet.setPlace_predicted(place_predicted);
        bet.setPlaced(placed);
        bet.setRace_id(race_id);
        bet.setWin_multiplier(win_multiplier);
        bet.setUserId(userId);

        producer.writeToKafka(bet);

        if (placed == null || win_multiplier == null || race_id == null || lose_multiplier == null || horse_id == null) {
            throw new Exception("Bad request");
        } else {
            sendEmail(user_request);
        }


        return true;

    }

}
