package com.stg.makeathon.userbetting.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Map;

@Service
public class MailContentBuilder {

    private TemplateEngine templateEngine;

    @Autowired
    public MailContentBuilder(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    public String build(Map<String, Object> details) {
        Context context = new Context();
        context.setVariable("horseName", details.get("horse_name"));
        context.setVariable("raceName", details.get("race_name"));
        context.setVariable("betAmount", details.get("placed"));
        context.setVariable("winMultiplier", details.get("win_multiplier"));
        return templateEngine.process("mailTemplate", context);
    }
//    {id=1, placed=200, win_multiplier=2, lose_multiplier=0.5, horse_id=2, place_predicted=1, race_id=1}


}