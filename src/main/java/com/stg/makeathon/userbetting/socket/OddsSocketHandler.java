package com.stg.makeathon.userbetting.socket;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.*;
import com.stg.makeathon.userbetting.domain.HorseDetailsRequestDTO;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class OddsSocketHandler extends TextWebSocketHandler {
    private List<WebSocketSession> sessions = new CopyOnWriteArrayList<>();
    private Map<WebSocketSession, Set<Integer>> sessionHorseMap = new HashMap<>();
    private ObjectMapper mapper = new ObjectMapper();
    //    private Set<Integer> horseSet = new HashSet<>();
    private Random random = new Random();

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        System.out.println(message.getPayload());
        HorseDetailsRequestDTO requestDTO = mapper.readValue(message.getPayload(), HorseDetailsRequestDTO.class);
        System.out.println("Requesting information for following horses: " + requestDTO.getHorses());
        Set<Integer> horses = new HashSet<>(requestDTO.getHorses());
        sessionHorseMap.put(session, horses);
//        horseSet.addAll(requestDTO.getHorses());
        session.sendMessage(getHorseDetails(horses));
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        sessions.add(session);
        System.out.println("Connected to: " + session.getId());
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        sessions.remove(session);
        sessionHorseMap.remove(session);
        System.out.println("Disconnected from: " + session.getId());
    }

    @Scheduled(fixedRate = 3000)
    private void sendUpdates() throws IOException {
        System.out.println(String.format("Sending updates to %d clients", sessions.size()));
        for (WebSocketSession session : sessionHorseMap.keySet()) {
            Set<Integer> horseSet = sessionHorseMap.get(session);
            System.out.println("Sending horse details for: " + horseSet + "to " + session.getId());
            TextMessage message = getHorseDetails(horseSet);
            session.sendMessage(message);
        }
    }


    private TextMessage getHorseDetails(Set<Integer> horseSet) {
        MongoClient mongo = new MongoClient("localhost", 27017);
        DB db = mongo.getDB("makeathon-db");
        DBCollection table = db.getCollection("horse_details");
        List<Map<String, Object>> horses = new ArrayList<>();
        for (int horseId : horseSet) {
            BasicDBObject searchQuery = new BasicDBObject();
            searchQuery.put("horse_id", horseId);
            DBObject cursor = table.findOne(searchQuery);
            Map<String, Object> horseDetails = cursor.toMap();
            double win_multiplier = Math.round(random.nextDouble() * 1000.0) / 100.0;
            double lose_multiplier = (1 / win_multiplier);
            horseDetails.put("win_multiplier", win_multiplier);
            horseDetails.put("lose_multiplier", lose_multiplier);
            horses.add(horseDetails);
        }
        mongo.close();
        try {
            Map<String, Object> response = new HashMap<>();
            response.put("horse_details", horses);
            return new TextMessage(mapper.writeValueAsString(response));
        } catch (IOException e) {
            return new TextMessage(e.getMessage());
        }
    }
}
