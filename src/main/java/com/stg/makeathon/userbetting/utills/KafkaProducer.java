package com.stg.makeathon.userbetting.utills;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.stg.makeathon.userbetting.domain.Bet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@EnableBinding(BetRegistrationSource.class)
@Service
public class KafkaProducer {
	@Autowired
    BetRegistrationSource betRegistrationSource;
    public String writeToKafka(Bet bet){
        betRegistrationSource.betRegistration().send(MessageBuilder.withPayload(bet).build());
        return "Registered";
    }
    
    @StreamListener("betSyncedChannel")
    public void readFromKafka(@Payload Bet bet){
        System.out.println("reading from kafka " + bet.getRace_id());
        
        MongoClient mongo = new MongoClient( "localhost" , 27017 );
		DB db = mongo.getDB("makeathon-db");
		DBCollection table = db.getCollection("user_details");

		String  userId = bet.getUserId();

		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("id", userId);

		DBObject cursor = table.findOne(searchQuery);

		Map<String,Object> userDetails =  cursor.toMap();

		List<Map<String,Object>> bets_placed = (List<Map<String,Object>>) userDetails.get("bets_placed");
        
        Map<String,Object> bets = new HashMap<String,Object>();
        bets.put("placed", bet.getPlaced());
	    bets.put("win_multiplier", bet.getWin_multiplier());
	    bets.put("lose_multiplier", bet.getLose_multiplier());
	    bets.put("horse_id", bet.getHorse_id());
	    bets.put("place_predicted", bet.getPlace_predicted());
	    bets.put("race_id", bet.getRace_id());
	    bets_placed.add(bets);

	    BasicDBObject newDocument = new BasicDBObject();
		newDocument.put("bets_placed", bets_placed);

		BasicDBObject updateObj = new BasicDBObject();
		updateObj.put("$set", newDocument);

		table.update(searchQuery, updateObj);
		System.out.println("finished writing");
		
    }
    
}
