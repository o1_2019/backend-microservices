package com.stg.makeathon.userbetting.utills;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface BetRegistrationSource {
	
    @Output("betRegistrationChannel")
    MessageChannel betRegistration();
    
    @Input("betSyncedChannel")
    SubscribableChannel betSync();
}
