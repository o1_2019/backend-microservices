package com.stg.makeathon.userbetting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class UserBettingApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserBettingApplication.class, args);
    }

}
